<nav id="top-links" class="nav toggle-wrap">
	<a href="#" class="toggle"><i class="material-icons-face"></i><span class="material-icons-arrow_drop_down"><?php echo $text_account; ?></span></a>
	<ul class="toggle_cont">
		<li><a href="<?php echo $wishlist; ?>"  title="<?php echo $text_wishlist; ?>" id="wishlist-total"><i class="material-icons-favorite_border"></i></a></li>
		<li class="toggle-wrap">
			<a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="toggle"><i class="material-icons-person"></i><span><?php echo $text_account; ?></span></a>
			<ul class="toggle_cont">
				<?php if ($logged) { ?>
				<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
				<li><a href="<?php echo $compare; ?>" id="compare-total2"><?php echo $text_compare; ?></a></li>
				<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
				<li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
				<li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
				<li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
				<?php } else { ?>
				<li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
				<li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
				<?php } ?>
			</ul>
		</li>
	</ul>
</nav>



