<div class="module-social">
	<div class="social-block">
		<h3 class="box-heading"><span><?php echo $title; ?></span></h3>
		<div class="box-content">
			<p><?php echo $description; ?></p>
			<?php if ($socials) { ?>
			<ul class="social-list list-unstyled">
				<?php foreach ($socials as $social) { ?>
				<li><a class="<?php echo $social['css']; ?>" href="<?php echo $social['link']; ?>" title="<?php echo $social['name']; ?>"></a></li>
				<?php } ?>
			</ul>
			<?php } ?>
		</div>
	</div>
</div>