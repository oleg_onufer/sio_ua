<div class="">
	<h3 class="box-heading"><span><?php echo $text_more; ?></span></h3>
	<ul class="box-content list-unstyled">
		<li><a href="<?php echo $store; ?>"><?php echo $text_store; ?></a></li>
		<li><a href="<?php echo $gift_cards; ?>"><?php echo $text_gift_cards; ?></a></li>
		<li><a href="<?php echo $catalog; ?>"><?php echo $text_catalog; ?></a></li>
	</ul>
</div>