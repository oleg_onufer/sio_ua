<div id="search" class="search">
	<input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" />
	<button type="button" class="button-search"><i class="material-icons-search"></i></button>
</div>