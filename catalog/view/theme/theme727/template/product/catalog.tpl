<?php echo $header; ?>

<div class="container">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>
	<div class="row">
		<div id="content" class="col-sm-12"><?php echo $content_top; ?>
			<h2><?php echo $heading_title; ?></h2>
			<?php if ($products) { ?>
			<div class="product-filter clearfix">
				<div class="product-filter_elem sort sort--category">
					<label class="control-label" for="input-category"><?php echo $text_sort_category; ?></label>
					<select id="input-category" class="form-control" onchange="location = this.value">
						<option value="<?php echo $all_categories; ?>"><?php echo $text_category; ?></option>
						<?php foreach ($categories as $category_1) { ?>
						<?php if ($category_1['category_id'] == $category_id) { ?>
						<option value="<?php echo $category_1['href']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $category_1['href']; ?>"><?php echo $category_1['name']; ?></option>
						<?php } ?>
						<?php foreach ($category_1['children'] as $category_2) { ?>
						<?php if ($category_2['category_id'] == $category_id) { ?>
						<option value="<?php echo $category_2['href']; ?>" selected="selected"><?php echo $category_2['name']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $category_2['href']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
						<?php } ?>
						<?php foreach ($category_2['children'] as $category_3) { ?>
						<?php if ($category_3['category_id'] == $category_id) { ?>
						<option value="<?php echo $category_3['href']; ?>" selected="selected"><?php echo $category_3['name']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $category_3['href']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
						<?php } ?>
						<?php } ?>
						<?php } ?>
						<?php } ?>
					</select>
				</div>
				
				<div class="product-filter_elem show pull-right">
					<label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
					<select id="input-limit" onchange="location = this.value;">
						<?php foreach ($limits as $limits) { ?>
						<?php if ($limits['value'] == $limit) { ?>
						<option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
						<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="row">
				<div id="mfilter-content-container" class="col-sm-5">
					<?php $pr = 10000; $clr = 0; foreach ($products as $product) { $pr++; $clr++; ?>
					<div class="catalog-thumb catalog-thumb--effect-julia" data-product="<?php echo $product['product_id']; ?>">
						<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
						<div class="catalog-thumb__overlay">
							<h2 class="catalog-thumb__title"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h2>
							<div>
								<?php if ($product['price']) { ?>
								<div class="catalog-item">
									<div class="price price-product">
										<?php if (!$product['special']) { ?>
										<?php echo $product['price']; ?>
										<?php } else { ?>
										<span class="price-new"><?php echo $product['special']; ?></span>
										<span class="price-old"><?php echo $product['price']; ?></span>
										<?php } ?>
										<?php if ($product['tax']) { ?>
										<span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
										<?php } ?>
									</div>
								</div>
								<br>
								<?php } ?>
								<?php if ($product['rating']) { ?>
								<div class="catalog-item">
									<div class="rating">
										<?php for ($i = 1; $i <= 5; $i++) { ?>
										<?php if ($product['rating'] < $i) { ?>
										<span class="fa-stack"><i class="material-icons-star fa-stack-1x"></i></span>
										<?php } else { ?>
										<span class="fa-stack"><i class="material-icons-star star fa-stack-1x"></i></span>
										<?php } ?>
										<?php } ?>
									</div>
								</div>
								<br>
								<?php } ?>
							</div>
						</div>
					</div>
					<?php } ?>
					<div id="spacer"></div>
				</div>
				<div class="col-sm-7">
					<div id="fixed_cnt-warp" class="catalog__cont-wrap">
						<div id="fixed_cnt" class="catalog__cont"></div>
					</div>
				</div>
			</div>
			<?php } else { ?>
			<p><?php echo $text_empty; ?></p>
			<div class="buttons">
				<div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
			</div>
			<?php } ?>
			<?php echo $content_bottom; ?>
		</div>
	</div>
</div>
<?php echo $footer; ?>

<script type="text/javascript">
	;(function ($) {
		$(document).ready(function(){
			var contentLeft   = $('#mfilter-content-container'),
			contentRight      = $('#fixed_cnt'),
			catalogThumbFirst = $('.catalog-thumb:first'),
			catalogThumbLast  = $('.catalog-thumb:last'),
			contentLeftOffsetTop,
			contentLefHeight,
			contentRightHeight,
			flag,
			itemIndex;

			$(window).load(function(){
				if ($(window).width() > 767) {
					contentLeftOffsetTop = contentLeft.offset().top,
					contentLefHeight     = contentLeftOffsetTop + contentLeft.outerHeight(),
					contentRightHeight   = contentRight.outerHeight(),
					flag                 = $(window).scrollTop() >= contentLeftOffsetTop - 15 && $(window).scrollTop() + contentRightHeight + 15 <= contentLefHeight;

					if ($(window).scrollTop() + ($(window).height()/4) < catalogThumbFirst.offset().top) {
						catalogThumbFirst.addClass('active');
						loadProduct(catalogThumbFirst.data('product'));
						itemIndex = catalogThumbFirst.data('product');
					} else if ($(window).scrollTop() + ($(window).height()/4) > catalogThumbLast.offset().top + catalogThumbLast.outerHeight()) {
						catalogThumbLast.addClass('active');
						loadProduct(catalogThumbLast.data('product'));
						itemIndex = catalogThumbLast.data('product');
					}
				}
			});

			$(window).on('load scroll resize', function(){
				if ($(window).width() > 767) {
					contentLeftOffsetTop = contentLeft.offset().top,
					contentLefHeight     = contentLeftOffsetTop + contentLeft.outerHeight(),
					contentRightHeight   = contentRight.outerHeight(),
					contentLeftItem      = $('.catalog-thumb'),
					catalogThumbFirst    = $('.catalog-thumb:first'),
					catalogThumbLast     = $('.catalog-thumb:last');
					$('#fixed_cnt-warp').css({
						'min-height' : contentLeft.outerHeight()
					});
					contentLeftItem.css({
						height: ($(window).height() - 45)/2
					});
					$('#spacer').css({
						height: ($(window).height() - 45)/2
					});
					if ($(window).scrollTop() >= contentLeftOffsetTop - 15 && $(window).scrollTop() + contentRightHeight + 15 <= contentLefHeight && flag) {
						flag = false;
						contentRight.removeClass('static absolute').addClass('fixed');
					} else if ($(window).scrollTop() < contentLeftOffsetTop - 15 && !flag) {
						flag = true;
						contentRight.removeClass('fixed absolute').addClass('static');
					} else if ($(window).scrollTop() + contentRightHeight + 15 > contentLefHeight && !flag) {
						flag = true;
						contentRight.removeClass('static fixed').addClass('absolute');
					}
					contentLeftItem.each(function(){
						if ($(window).scrollTop() + ($(window).height()/4) >= $(this).offset().top && $(window).scrollTop() + ($(window).height()/4) <= $(this).offset().top + $(this).outerHeight()) {
							if (!$(this).hasClass('active') || itemIndex != $(this).data('product')) {
								$(this).addClass('active');
								loadProduct($(this).data('product'));
								itemIndex = $(this).data('product');
							}
						} else if (
							$(this).hasClass('active')
							&&
							$(window).scrollTop() + ($(window).height()/4) >= catalogThumbFirst.offset().top
							&&
							$(window).scrollTop() + ($(window).height()/4) <= catalogThumbLast.offset().top + catalogThumbLast.outerHeight()
							) {
							$(this).removeClass('active');
						}
					});
				}
			});
		});
		function loadProduct(product_id) {
			$.ajax({
				type: "POST",
				cache: false,
				url: 'index.php?route=product/catalog/AjaxDetails',
				data: {
					product_id: product_id
				},
				beforeSend: function() {
					$('#fixed_cnt').addClass('loading');
				},
				success: function(data){
					$('#fixed_cnt').html(data).removeClass('loading');
				}
			});
		}
	})(jQuery);
</script>