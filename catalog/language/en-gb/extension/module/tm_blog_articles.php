<?php

// Heading 
$_['heading_title']        = 'Blog';

$_['text_date_format']     = 'd/m/Y';

$_['text_popular_all']     = 'Popular Articles';
$_['text_latest_all']      = 'Latest from the Blog';
$_['text_button_continue'] = 'Read More';
$_['text_comments']        = ' Comments';
$_['text_comment']         = ' Comment';
$_['text_posted']         = 'Posted ';
$_['text_by']         = 'by ';

$_['text_no_result']       = 'No Result!';

?>