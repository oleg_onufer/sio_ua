<?php
class ControllerExtensionModuleTMSetting extends Controller {
	public function index() {
		$this->load->language('common/header');
		
		$data['text_sett']         = $this->language->get('text_sett');
		
		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		
		return $this->load->view('extension/module/tm_setting', $data);
	}
}