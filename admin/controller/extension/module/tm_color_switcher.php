<?php
class ControllerExtensionModuleTMColorSwitcher extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/tm_color_switcher');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->model_setting_setting->editSetting('tm_color_switcher', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true));
		}

		$data['heading_title']      = $this->language->get('heading_title');
		
		$data['text_edit']          = $this->language->get('text_edit');
		$data['text_enabled']       = $this->language->get('text_enabled');
		$data['text_disabled']      = $this->language->get('text_disabled');
		
		$data['entry_status']       = $this->language->get('entry_status');
		$data['entry_color_scheme'] = $this->language->get('entry_color_scheme');
		$data['entry_title']        = $this->language->get('entry_title');
		
		$data['button_save']        = $this->language->get('button_save');
		$data['button_cancel']      = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
			);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true)
			);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/tm_color_switcher', 'token=' . $this->session->data['token'], true)
			);

		$data['action'] = $this->url->link('extension/module/tm_color_switcher', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);

		if (isset($this->request->post['tm_color_switcher_status'])) {
			$data['tm_color_switcher_status'] = $this->request->post['tm_color_switcher_status'];
		} else {
			$data['tm_color_switcher_status'] = $this->config->get('tm_color_switcher_status');
		}

		if (isset($this->request->post['tm_color_switcher_scheme'])) {
			$data['tm_color_switcher_scheme'] = $this->request->post['tm_color_switcher_scheme'];
		} elseif ($this->config->get('tm_color_switcher_scheme')) {
			$data['tm_color_switcher_scheme'] = $this->config->get('tm_color_switcher_scheme');
		} else {
			$data['tm_color_switcher_scheme'] = 'color_scheme_1';
		}

		$color_schemes = glob(DIR_CATALOG . 'view/theme/' . $this->config->get($this->config->get('config_theme') . '_directory') . '/stylesheet/color_schemes/*');

		$i = 0; foreach ($color_schemes as $scheme) {
			$data['color_schemes'][$i] = array(
				'scheme' => preg_replace('/\\.[^.\\s]{3,4}$/', '', explode('color_schemes/', $scheme)[1]),
				'name' => 'Color Scheme' . ' ' . ($i + 1)
				);
			$i++;
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/tm_color_switcher', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/tm_color_switcher')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}