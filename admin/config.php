<?php
// HTTP
define('HTTP_SERVER', 'http://sio.ua/admin/');
define('HTTP_CATALOG', 'http://sio.ua/');

// HTTPS
define('HTTPS_SERVER', 'http://sio.ua/admin/');
define('HTTPS_CATALOG', 'http://sio.ua/');

// DIR
define('DIR_APPLICATION', '/home/olegsoye/sio.ua/admin/');
define('DIR_SYSTEM', '/home/olegsoye/sio.ua/system/');
define('DIR_IMAGE', '/home/olegsoye/sio.ua/image/');
define('DIR_LANGUAGE', '/home/olegsoye/sio.ua/admin/language/');
define('DIR_TEMPLATE', '/home/olegsoye/sio.ua/admin/view/template/');
define('DIR_CONFIG', '/home/olegsoye/sio.ua/system/config/');
define('DIR_CACHE', '/home/olegsoye/sio.ua/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/olegsoye/sio.ua/system/storage/download/');
define('DIR_LOGS', '/home/olegsoye/sio.ua/system/storage/logs/');
define('DIR_MODIFICATION', '/home/olegsoye/sio.ua/system/storage/modification/');
define('DIR_UPLOAD', '/home/olegsoye/sio.ua/system/storage/upload/');
define('DIR_CATALOG', '/home/olegsoye/sio.ua/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'DB_USERNAME');
define('DB_PASSWORD', 'DB_PASSWORD');
define('DB_DATABASE', 'DB_DATABASE');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
