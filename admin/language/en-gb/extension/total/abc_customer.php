<?php
// Heading
$_['heading_title']    = 'ABC Customer (customer group discount)';

// Text
$_['text_total']       = 'Order Totals';
$_['text_success']     = 'Success: You have modified Discount total!';

// Entry
$_['entry_discount']      = 'Customers Discount:';
$_['entry_discount_help'] = 'Set percent discount for customers groups';
$_['entry_status']     = 'Status:';
$_['entry_sort_order'] = 'Sort Order:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Discount total!';
?>