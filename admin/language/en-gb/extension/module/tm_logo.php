<?php
// Heading
$_['heading_title']    = 'TemplateMonster Logo';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified TemplateMonster Logo module!';
$_['text_edit']        = 'Edit TemplateMonster Logo Module';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_status']     = 'Status';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify TemplateMonster Logo module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';